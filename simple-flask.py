from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/first')
def hello_world2():
    return 'Hello World first!'

@app.route('/second')
def hello_world3():
    return 'Hello World second!'


if __name__ == '__main__':
    app.run()
